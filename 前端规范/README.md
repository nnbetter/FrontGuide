# web通用规范

## 一般规范

### 文件/资源名

1. 所有的文件名应该都遵循同一命名约定。以可读性而言，减号（-）是用来分隔文件名的不二之选

2. URL分隔符
>//example.com/blog/my-blog-entry 

>//s.example.com/images/big-black-background.jpg

3. 文件命名总是以字母开头而不是数字

4. 资源的字母名称必须全为小写，因为某些系统对大小写敏感。

5. 需要对文件增加前后缀或特定的扩展名（比如 .min.js, .min.css），抑或一串前缀（比如 3fa89b.main.min.css）。这种情况下，建议使用**点分隔符**来区分这些在文件名中带有清晰意义的元数据。

### 协议

不要指定引入资源所带的具体协议，在请求资源协议无法确定时非常好用。
>`<script src="//cdn.com/foundation.min.js"></script>`

### 注释

不要写你的代码都干了些什么，而要写你的代码为什么要这么写，背后的考量是什么。当然也可以加入所思考问题或是解决方案的链接地址。

### 代码检查
推荐使用ESLint

### 编辑器
推荐使用 [**vscode**](https://code.visualstudio.com/)

### 代码风格
1. 用两个空格代替制表符（soft-tab 即用空格代表 tab 符）。
1. 保存文件时，删除尾部的空白符。
1. 设置文件编码为 UTF-8。
1. 在文件结尾添加一个空白行。

>参照文档并将这些配置信息添加到项目的 .editorconfig 文件中。例如：Bootstrap 中的 [.editorconfig](https://github.com/twbs/bootstrap/blob/master/.editorconfig) 实例。更多信息请参考 [about EditorConfig](http://editorconfig.org/)。

>vscode、brackets、atom、webstorm等编辑中都有EditorConfig插件，推荐使用EditorConfig保证代码格式、风格的统一。

## HTML规范

### 语法
对于属性的定义，确保全部使用双引号，绝不要使用单引号。

### 文档类型
推荐使用 HTML5 的文档类型申明： `<!DOCTYPE html>`
``` html
<!DOCTYPE html>
<html lang="zh-CN">
  ...
</html>
```

###  命名
1. class 必须单词全字母小写，单词间以 - 分隔。
1. class 必须代表相应模块或部件的内容或功能，不得以样式信息进行命名。
``` html
<!-- 推荐 -->
<div class="sidebar"></div>

<!-- 不推荐 -->
<div class="left"></div>
```
1. 元素 id 必须保证页面唯一。
1. id 建议单词全字母小写，单词间以 - 分隔。同项目必须保持风格一致。
1. 同一页面，应避免使用相同的 name 与 id。IE 浏览器会混淆元素的 id 和 name 属性， document.getElementById 可能获得不期望的元素。所以在对元素的 id 与 name 属性的命名需要非常小心。
1. 标签名必须使用小写字母。
1. 布尔类型的属性，建议不添加属性值
``` html
<input type="text" disabled>
<input type="checkbox" value="1" checked>
```

### IE 兼容模式
IE 支持通过特定的 <meta> 标签来确定绘制当前页面所应该采用的 IE 版本。
``` html
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
```

### 字符编码
一律采用**UTF-8**
``` html
<head>
  <meta charset="UTF-8">
</head>
```

### 属性顺序
class 用于标识高度可复用组件，因此应该排在首位。id 用于标识具体组件，应当谨慎使用（例如，页面内的书签），因此排在第二位。
1. class
1. id, name
1. data-*
1. src, for, type, href, value
1. title, alt
1. role, aria-*
```html
<a class="..." id="..." data-toggle="modal" href="#">
  Example link
</a>

<input class="form-control" type="text">

<img src="..." alt="...">
```

### 脚本加载
js脚本其加载会一直阻塞 DOM 解析，直至它完全地加载和执行完毕，这会造成页面显示的延迟。
**所有浏览器中，推荐**
``` html
<html>
  <head>
    <link rel="stylesheet" href="main.css">
  </head>
  <body>
    <!-- 内容 -->
 
    <script src="main.js" async></script>
  </body>
</html>
```

### 减少标签的数量
编写 HTML 代码时，尽量避免多余的父元素。
```html
<!-- 推荐 -->
<img class="avatar" src="image.png">

<!-- 不推荐 -->
<span class="avatar">
    <img src="image.png">
</span>
```

### 语义化
根据元素（有时被错误地称作“标签”）其被创造出来时的初始意义来使用它。h元素来定义标题，p 元素来定义文字段落，用 a 元素来定义链接锚点，等等。
下面是常见标签语义

1. p - 段落
1. h1,h2,h3,h4,h5,h6 - 层级标题
1. strong,em - 强调
1. ins - 插入
1. del - 删除
1.  abbr - 缩写
1.  code - 代码标识
1.  cite - 引述来源作品的标题
1.  q - 引用
1.  blockquote - 一段或长篇引用
1.  ul - 无序列表
1. ol - 有序列表
1. dl,dt,dd - 定义列表

## CSS规范

### 语法
1. 省略“0”值后面的单位。不要在0值后面使用单位，除非有值。
```css
/* 不推荐 */
padding-bottom: 0px;
margin: 0em;
/* 推荐 */
padding-bottom: 0;
margin: 0;
```
2. 对于属性值或颜色参数，省略小于 1 的小数前面的 0 （例如，.5 代替 0.5；-.5px 代替 -0.5px）。
1. 十六进制值应该全部小写，例如，#fff。在扫描文档时，小写字符易于分辨，因为他们的形式更易于区分。
1. 尽量使用简写形式的十六进制值，例如，用 #fff 代替 #ffffff。
1. 属性选择器或属性值用双引号（””），而不是单引号（”）括起来。

### 跨浏览器表现一致性
推荐使用 [**Normalize.css**](http://necolas.github.io/normalize.css/) CSS 重置样式库

### ID和Class的命名
1. 应该首选具体和反映元素目的的名称，因为这些是最可以理解的，而且发生变化的可能性最小。
```css
/* 不推荐 */
.fw-800 {
  font-weight: 800;
}
 
.red {
  color: red;
}
/* 推荐 */
.heavy {
  font-weight: 800;
}
 
.important {
  color: red;
}
```
2. 使用连字符（中划线）分隔ID和Class（类）名中的单词。为了增强课理解性，在选择器中不要使用除了连字符（中划线）以为的任何字符（包括没有）来连接单词和缩写。
```css
/* 不推荐 */
.demoimage {}
.error_status {}
/* 推荐 */
#video-id {}
.ads-sample {}
```
3. 使用 .js-* class 来标识行为（与样式相对），并且不要将这些 class 包含到 CSS 文件中。


### 合理的避免使用ID
1. 一般情况下ID不应该被应用于样式。
1. 使用ID唯一有效的是确定网页或整个站点中的位置。
1. 你应该始终考虑使用class，而不是id，除非只使用一次。

```css
/* 不推荐 */
#content .title {
  font-size: 2em;
}
/* 推荐 */
.content .title {
  font-size: 2em;
}
```

### 选择器
1. 当构建选择器时应该使用清晰， 准确和有语义的class(类)名。不要使用标签选择器。
1. 不使用元素选择器，那么你只需要改变你的html标记，而不用改动你的CSS。
```css
/* 不推荐 */
div.content > header.content-header > h2.title {
  font-size: 2em;
}
/* 推荐 */
.content > .content-header > .title {
  font-size: 2em;
}
```
3. 对于经常出现的组件，避免使用属性选择器（例如，[class^="..."]）。浏览器的性能会受到这些因素的影响。

4. 选择器要尽可能短，并且尽量限制组成选择器的元素个数，建议不要超过 3 。

5. 如果你不写很通用的，需要匹配到DOM末端的选择器， 你应该总是考虑直接子选择器。
```css
/* 不推荐 */
.content .title {
  font-size: 2rem;
}
/* 推荐 */
.content > .title {
  font-size: 2rem;
}
 
.content > .content-body > .title {
  font-size: 1.5rem;
}
 
.content > .content-body > .teaser > .title {
  font-size: 1.2rem;
}
```

### 简写形式的属性声明
在不需要显示地设置所有值的情况下，应当尽量限制使用简写形式的属性声明。常见的滥用简写属性声明的情况如下：
* padding
* margin
* font
* background
* border
* border-radius

大部分情况下，我们不需要为简写形式的属性声明指定所有值。例如，HTML 的 heading 元素只需要设置上、下边距（margin）的值，因此，在必要的时候，只需覆盖这两个值就可以。过度使用简写形式的属性声明会导致代码混乱，并且会对属性值带来不必要的覆盖从而引起意外的副作用。
```css
/* 不推荐 */
.element {
  margin: 0 0 10px;
  background: red;
  background: url("image.jpg");
  border-radius: 3px 3px 0 0;
}

/* 推荐 */
.element {
  margin-bottom: 10px;
  background-color: red;
  background-image: url("image.jpg");
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
```

### 声明顺序
这是一个选择器内书写CSS属性顺序的大致轮廓。这是为了保证更好的可读性和可扫描重要。

作为最佳实践，我们应该遵循以下顺序（应该按照下表的顺序）：
1. 结构性属性：
* display
* position, left, top, right etc.
* overflow, float, clear etc.
* margin, padding
2. 表现性属性：
* background, border etc.
* font, text

```css
/* 不推荐 */
.box {
  font-family: 'Arial', sans-serif;
  border: 3px solid #ddd;
  left: 30%;
  position: absolute;
  text-transform: uppercase;
  background-color: #eee;
  right: 30%;
  isplay: block;
  font-size: 1.5rem;
  overflow: hidden;
  padding: 1em;
  margin: 1em;
}
/* 推荐 */
.box {
  display: block;
  position: absolute;
  left: 30%;
  right: 30%;
  overflow: hidden;
  margin: 1em;
  padding: 1em;
  background-color: #eee;
  border: 3px solid #ddd;
  font-family: 'Arial', sans-serif;
  font-size: 1.5rem;
  text-transform: uppercase;
}
```

### 代码组织
1. 以组件为单位组织代码段。
1. 使用两行空白符将组件/模块代码分隔成块，这样利于扫描较大的文档。
1. 如果使用了多个 CSS 文件，将其按照组件而非页面的形式分拆，因为页面会被重组，而组件只会被移动。

## JavaScript规范

### 语法
1. 统一使用单引号(‘)，不使用双引号(“)。这在创建 HTML 字符串非常有好处：
```javascript
 var msg = 'This is some HTML <div class="makes-sense"></div>';
```
2. 总是使用 var 来声明变量。如不指定 var，变量将被隐式地声明为全局变量，这将对变量难以控制。
3. 只用一个 var 关键字声明，多个变量用逗号隔开。
```js
//不推荐
x = 10;
y = 100;
//推荐
var x = 10,
    y = 100;
```
4. 所有的变量以及方法，应当定义在 function 内的首行。
```js
//不推荐
(function(log){
  'use strict';
 
  var a = 10;
  var b = 10;
 
  for(var i = 0; i < 10; i++) {
    var c = a * b * i;
  }
 
  function f() {
 
  }
 
  var d = 100;
  var x = function() {
    return d * d;
  };
  log(x());
 
}(window.console.log));
//推荐
(function(log){
  'use strict';
 
  var a = 10,
      b = 10,
      i,
      c,
      d,
      x;
 
  function f() {
 
  }
 
  for(i = 0; i < 10; i++) {
    c = a * b * i;
  }
 
 
 
  d = 100;
  x = function() {
    return d * d;
  };
  log(x());
 
}(window.console.log));
``` 


### 全局命名空间污染与 IIFE
 使用IIFE(Immediately-Invoked Function Expression)，用以创建独立隔绝的定义域，避免全局污染。
 ```js
//不推荐
 var x = 10,
    y = 100;
 
//这样声明的变量会污染全局作用域
console.log(window.x + ' ' + window.y);

//推荐
//通过IIFE（自调用函数）
(function(log, w, undefined){
  'use strict';
 
  var x = 10,
      y = 100;
 
  // 将会输出 'true true'
  log((w.x === undefined) + ' ' + (w.y === undefined));
 
}(window.console.log, window));
 ```

### IIFE（立即执行的函数表达式）
立即执行的函数表达式的执行括号应该写在外包括号内。虽然写在内还是写在外都是有效的，但写在内使得整个表达式看起来更像一个整体。
```js
//不推荐
(function(){})();
//推荐
(function(){}());
```

### 总是使用带类型判断的比较判断
总是使用 === 精确的比较操作符，避免在判断的过程中，由 JavaScript 的强制类型转换所造成的困扰。

### 语句块内的函数声明
切勿在语句块内声明函数，在 ECMAScript 5 的严格模式下，这是不合法的。
```js
//不推荐
if (x) {
  function foo() {}
}
//推荐
if (x) {
  var foo = function() {};
}
```

### 标准特性
总是优先考虑使用标准特性。为了最大限度地保证扩展性与兼容性，总是首选标准的特性，而不是非标准的特性（例如：首选 string.charAt(3) 而不是 string[3]；首选 DOM 的操作方法来获得元素引用，而不是某一应用特定的快捷方法）。

### 切勿在循环中创建函数
在简单的循环语句中加入函数是非常容易形成闭包而带来隐患的。
```js
//不推荐
(function(log, w){
  'use strict';
 
  var numbers = [1, 2, 3],
      i;
 
  for(i = 0; i < numbers.length; i++) {
    w.setTimeout(function() {
      w.alert('Index ' + i + ' with number ' + numbers[i]);
    }, 0);
  }
 
}(window.console.log, window));
//推荐
(function(log, w){
  'use strict';
 
  var numbers = [1, 2, 3],
      i;
 
  numbers.forEach(function(number, index) {
    w.setTimeout(function() {
      w.alert('Index ' + index + ' with number ' + number);
    }, 0);
  });
 
}(window.console.log, window));
```

### eval 函数（魔鬼）
eval() 不但混淆语境还很危险，总会有比这更好、更清晰、更安全的另一种方案来写你的代码，因此尽量不要使用 evil 函数。

### 首选函数式风格
函数式编程让你可以简化代码并缩减维护成本，因为它容易复用，又适当地解耦和更少的依赖。
```js
//不推荐
(function(log){
  'use strict';
 
  var arr = [10, 3, 7, 9, 100, 20],
      sum = 0,
      i;
 
 
  for(i = 0; i < arr.length; i++) {
    sum += arr[i];
  }
 
  log('The sum of array ' + arr + ' is: ' + sum)
 
}(window.console.log));
//推荐
(function(log){
  'use strict';
 
  var arr = [10, 3, 7, 9, 100, 20];
 
  var sum = arr.reduce(function(prevValue, currentValue) {
    return prevValue + currentValue;
  }, 0);
 
  log('The sum of array ' + arr + ' is: ' + sum);
 
}(window.console.log));
```

### 不要使用 switch
switch 在所有的编程语言中都是个非常错误的难以控制的语句，建议用 if else 来替换它。

### 数组和对象字面量
```js
//不推荐
var a1 = new Array(x1, x2, x3);
var a2 = new Array(x1, x2);
var o2 = new Object();
o2.a = 0;
o2.b = 1;
o2.c = 2;
o2['strange key'] = 3;

//推荐
var a = [x1, x2, x3];
var a2 = [x1, x2];
var a3 = [x1];
var a4 = [];
var o = {};
var o2 = {
  a: 0,
  b: 1,
  c: 2,
  'strange key': 3
};
```




