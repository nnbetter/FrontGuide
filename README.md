# 前端规范工作指引 :tw-1f1e8-1f1f3: 

>本指引实乃仓促之作在下才能有限，如果有任何问题可以PullRequests和提Issues。

>原则：--追求简单易用、简洁。--因为是做一些通用的东西，为的就是方便开发者，封装的粒度之间需要做一个权衡，常出现的一种情况就是封装了太多的方法和库，使用者在使用的时候往往找不到文档，很影响开发效率。使用一些比较出名的第三方库是个不错的选择，知名度好，文档齐全，编码统一。

    自从HTML5出来以后，前端就开始火了，再加上移动端近年来发展的趋势，愈来愈火。最开始是没有前端这个概念的，远古时期的“前端”其实是由后端人员完成的，因为那是的页面很简单并不复杂。现在不一样了，用户对前端页面的审美需求、页面的复杂性（比如电商页面）越来越高，所以前端就独立出来了~奉劝各位非特别需要不要再去研究和IE浏览器相关的技术了，毕竟把时间浪费在这些过时的技术上面还不如多去外面走走看看祖国的大好山河。

    传统：页面由后端渲染，和后端的耦合性很强，甚至页面还嵌套有服务器端的代码。IE时代的特殊性，出现了跨浏览器统一的JQuery，JQuery很受欢迎，那时候世界上45%的网站都使用了它。所以那时候的方式就是HTML+css+jquery

    Ajax：ajax个人理解为一句话就是：局部获取页面部分数据。传统由后端渲染的页面是同步的方式，不如点击下一页的时候整个页面都会刷新，数据是同步展现出来的。ajax出现后，就不用整个页面刷新了，传输的数据也就更少更快了，这样用户体验操作就好了。现在任然有很多网站使用后端渲染（同步）+ajax的方式（异步）开发网站。

    SPA：（Single Page Application）单页面应用程序的出现，是目前最流行的方式，各种mvvm框架层出不穷，如Angular、React、Vue......以数据驱动的方式，面向接口的方式，比较真正意义上的和后端解耦了。
    优势：
    1.前后端职责、关注点分离,架构清晰，方便维护。
    2.后端由于是面向接口的，可以多个端同时使用
    3.最重要的是用户体验好
    当然也有缺点，比如不利于SEO等，放心这些都有解决方案。

    --伍冠源 2018-1-10

> 

## 1.前端规范
- [前端规范](https://gitee.com/nnbetter/FrontGuide/blob/master/%E5%89%8D%E7%AB%AF%E8%A7%84%E8%8C%83/README.md)
- [代码风格指南](https://gitee.com/nnbetter/FrontGuide/tree/master/%E4%BB%A3%E7%A0%81%E9%A3%8E%E6%A0%BC%E6%8C%87%E5%8D%97)

## 2.前端技术栈
以下工具和框架要熟悉使用，详细使用教程麻烦自行百度（网上有很多资源）。抛弃传统的方式面向SPA单页应用ba(～￣▽￣)～ 
> vue+vue-cli+vue-router+vuex 俗称vue全家桶系列~
- [Node.js](https://nodejs.org/en/)：简单的说 Node.js 就是运行在服务端的 JavaScript。前端构建环境的搭建是基于Node的，前端只要下载安装环境即可，不需要深入学习。
- [npm](https://www.npmjs.com/)：node的包管理器，上面有成千上万的第三方包。
- [vue](https://cn.vuejs.org/)：是一套用于构建用户界面的**渐进式**框架（官网上说的~）
- [webpack](https://doc.webpack-china.org/)：是一个静态模块打包器，可用于当作前端构建工具使用。
- [vue-cli](https://github.com/vuejs/vue-cli)：官方命令行工具，可用于快速搭建大型单页应用。其实就是一脚手架工具，使用一行命令就可以自动创建vue项目，自带目录结构和项目构建工具依赖,请使用webpack构建。
- [vue-router](https://router.vuejs.org/zh-cn/)：路由框架，单页面当然要有自己的路由~
- [vuex](https://vuex.vuejs.org/zh-cn/)：Vuex 是一个专为 Vue.js 应用程序开发的**状态管理模式**,用于复杂应用间的状态管理。
- [axios](https://github.com/axios/axios)：Axios 是一个基于 promise 的 HTTP 库，可以用在浏览器和 node.js 中。前端使用它来发送ajax请求~
- [Sass](https://www.sass.hk/)：css预编译语言
- [Git](https://git-scm.com/)：Git是目前世界上最先进的分布式版本控制系统（没有之一）。

### 参考：
- [NPM 使用介绍](http://www.runoob.com/nodejs/nodejs-npm.html)

## 3.vue-admin
项目地址：[vue-admin](https://gitee.com/mrfive/vue-admin)（请先联系我获取权限）

使用[Element](http://element-cn.eleme.io/#/zh-CN)搭建的一个后台框架。并没有进行过多的封装，目的是方便易于学习和使用。当然你也可以自由发挥，搭建自己喜欢的后台框架。

## 4.vue学习指南
1. 首先跟着[vue官网](https://cn.vuejs.org/)把文档和官网的例子都写一遍
2. 查看网上的教程，理解整个vue的体系从环境搭建到发布。
    - [Vue2.0 新手完全填坑攻略—从环境搭建到发布](https://jinkey.ai/post/tech/vue2.0-xin-shou-wan-quan-tian-keng-gong-lue-cong-huan-jing-da-jian-dao-fa-bu)
    - [vue项目构建与实战](https://www.jianshu.com/p/78e10c30e159)
    - [手摸手，带你用vue撸后台系列](https://juejin.im/post/59097cd7a22b9d0065fb61d2)
3. 查看别人的例子
    - [vue2-echo](https://github.com/uncleLian/vue2-echo) 这个作者不错~
    - [Vue News](https://github.com/daoket/vue.news)
    - [vue-buy-tickets](https://github.com/hj0503/vue-buy-tickets)
    - [lvyou](https://github.com/LeachZhou/lvyou)

    更多例子：[知乎上的vue.js实例项目](https://www.zhihu.com/question/37984203)、[Vue相关开源项目库集合](https://github.com/opendigg/awesome-github-vue)
4. 查漏补缺
    - [Vue 脱坑记 - 查漏补缺(汇总下群里高频询问的xxx及给出不靠谱的解决方案)](https://juejin.im/post/59fa9257f265da43062a1b0e)
 

## 5.vue资源
- [vue官方资源awesome-vue](https://github.com/vuejs/awesome-vue) ：基本上这个可以满足你99%的需求了。
- [VUX](https://vux.li/#/)：WeUI + Vue + Webpack
- [Mint UI](https://github.com/ElemeFE/mint-ui/)：由饿了么前端团队推出的 Mint UI 是一个基于 Vue.js 的移动端组件库。
- [Muse UI](https://github.com/museui/muse-ui)：基于 Vue 2.0 和 Material Desigin 的 UI 组件库
- [cube-ui](https://github.com/didi/cube-ui)：滴滴团队开发的一套基于 Vue.js 实现的精致移动端组件库
- [vant
](https://github.com/youzan/vant)：有赞开发的基于 Vue.js 2.0 的 UI 组件库(**可用于电商**)
### 移动端
- [vue-modal](https://github.com/mengchen129/vue-modal)：基于 Vue.js 2.0 编写的弹窗组件
- [wc-messagebox](https://github.com/helicopters/wc-messagebox)：基于 Vue 2.0 开发的 Alert, Toast, Confirm 插件, UI仿照 iOS 原生
- [vue-picker](https://github.com/naihe138/vue-picker)：走了一圈 github 都没有找到自己想要的移动端的 vue-picker的组件，于是自己就下手，撸了一个出来
- [pd-select](https://github.com/k186/pd-select/tree/master)：vue components ,like ios 3D picker style,vue 3d 选择器组件

## 6.JQuery/Javascript资源
一些项目由于历史遗留问题，任然还在使用JQuery等库开发项目，没办法咯~在这里奉献上一些我们在实际项目中常用的资源、插件。
### 移动端
- [picker](https://github.com/ustbhuangyi/picker)：移动端最好用的的筛选器组件，高仿 ios 的 UIPickerView ，非常流畅的体验。
- [Picker.js](https://github.com/fengyuanchen/pickerjs)：时间选择器
- [mobile-select-area](https://github.com/tianxiangbing/mobile-select-area)：城市地区选择器
- [Swiper](http://www.swiper.com.cn/)：Swiper常用于移动端网站的内容触摸滑动
- [TouchSlide](http://www.superslide2.com/TouchSlide/index.html)：TouchSlide 是纯javascript打造的触屏滑动特效插件，面向手机、平板电脑等移动终端，能实现触屏焦点图、触屏Tab切换、触屏多图切换等常用效果。
- [iSlider](http://be-fe.github.io/iSlider/demo/index_chinese.html#demo)：iSlider是一个表现出众，无任何插件依赖的手机平台javascript滑动组件。
### PC端
PC端的网上太多了，找一个自喜欢的就好了，还是说你有什么好的推荐呢？

## 7.页面布局

1. 常规方式：一般的存PC端页面都使用这种方式，根据设计稿的实际像素作页面是多少就是多少排序px作为主要使用单位。
2. 响应式布局：PC和移动端同用一套样式代码。简而言之，就是一个网站能够兼容多个终端——而不是为每个终端做一个特定的版本。主要使用css3中的Media Query（媒介查询）实现。
3. 流式布局：宽度使用百分比%，使用px作为css的单位。只适用于一些简单的页面如列表、表格......
4. rem布局：相对于网站的根元素的font-size等比例缩放页面元素。
5. flex布局：弹性布局也称盒子布局,**Flex 布局将成为未来布局的首选方案**。
> 移动端优先使用flex>rem>百分比

rem布局使用：
1. 复制以下代码到HTML页面head中，因为是关乎到布局因素所以一定要放到最前面。
```js
(function(e,h){var g=document,d=window;var b=g.documentElement;var c;var i,a;function f(){var j=b.getBoundingClientRect().width;var k=window.devicePixelRatio||1;b.setAttribute("data-dpr",k);if(!h){h=750}if(j>h){j=h}var m=j*100/e;a="html{font-size:"+m+"px !important}";i=document.getElementById("rootsize")||document.createElement("style");if(!document.getElementById("rootsize")){document.getElementsByTagName("head")[0].appendChild(i);i.id="rootsize"}if(i.styleSheet){i.styleSheet.disabled||(i.styleSheet.cssText=a)}else{try{i.innerHTML=a}catch(l){i.innerText=a}}b.style.fontSize=m+"px"}f();d.addEventListener("resize",function(){clearTimeout(c);c=setTimeout(f,300)},false);d.addEventListener("pageshow",function(j){if(j.persisted){clearTimeout(c);c=setTimeout(f,300)}},false);if(g.readyState==="complete"){g.body.style.fontSize="16px"}else{g.addEventListener("DOMContentLoaded",function(j){g.body.style.fontSize="16px"},false)}})(750,768);//750为设计稿的宽度，768为设置页面的最大宽度
```
2. 如页面中元素的宽度为60px,只需要除100即可,那么宽度就为0.6rem.如设计稿宽度为750px,那么可以再css中设置页面的最大宽度为7.5rem。

## 8.其它
1. 推荐使用ES6/7语法，兼容低版本浏览器结合webpack使用[babel](https://babeljs.cn/)转换即可。

## 9.待解决问题
1. vue的SEO服务端渲染的问题。==>[Vue.js 服务器端渲染指南](https://ssr.vuejs.org/zh/)
