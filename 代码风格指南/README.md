# 代码风格指南

1. [JavaScript语法规范](https://github.com/standard/standard/blob/master/docs/RULES-zhcn.md)
1. 建议编辑器使用 [vscode](https://code.visualstudio.com/Download)，可结合下文中的Eslint和Editconfig作为vscode插件使用
1. 本文件夹有推荐使用的.editorconfig、.eslintrc.js、.eslintignore 文件

## [Eslint](http://eslint.cn/)
1. 用于帮助我们检查Javascript编程的语法错误.，可避免git冲突
1. [Eslint规则说明](https://gitee.com/nnbetter/FrontGuide/blob/master/%E4%BB%A3%E7%A0%81%E9%A3%8E%E6%A0%BC%E6%8C%87%E5%8D%97/Eslint%E8%A7%84%E5%88%99%E8%AF%B4%E6%98%8E.md)
1. .eslintignore用于排除不用代码检查的文件

.eslintrc.js:
```js
// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: 'standard',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // add your custom rules here
  'rules': {
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'no-undef': 0,
    'indent': 0, 
    'camelcase': 0,
    'space-before-function-paren':0,
    'quotes': 0
  }
}

```
.eslintignore:
```
build/*.js
config/*.js
```

## [Editconfig](http://editorconfig.org/)
 Editconfig 定义和维护一致的编码风格。
### 通配符
通配符|说明
-|-|-
* |	匹配除/之外的任意字符串
** |	匹配任意字符串
? |	匹配任意单个字符
[name] |	匹配name字符
[!name] |	匹配非name字符
{s1,s3,s3} |	匹配任意给定的字符串（0.11.0起支持）
特殊字符可以用\转义，以使其不被认为是通配符。
### 属性
indent_style: 设置缩进风格，tab或者空格。tab是hard tabs，space为soft tabs。

indent_size: 缩进的宽度，即列数，整数。如果indent_style为tab，则此属性默认为tab_width。

tab_width: 设置tab的列数。默认是indent_size。

end_of_line： 换行符，lf、cr和crlf

charset： 编码，latin1、utf-8、utf-8-bom、utf-16be和utf-16le，不建议使用utf-8-bom。

trim_trailing_whitespace： 设为true表示会除去换行行首的任意空白字符。

insert_final_newline: 设为true表明使文件以一个空白行结尾

root: 表明是最顶层的配置文件，发现设为true时，才会停止查找.editorconfig文件。

[更多属性](https://github.com/editorconfig/editorconfig/wiki/EditorConfig-Properties)

```js
root = true

[*]
charset = utf-8
indent_style = space
indent_size = 2
end_of_line = lf
insert_final_newline = true
trim_trailing_whitespace = true
```
